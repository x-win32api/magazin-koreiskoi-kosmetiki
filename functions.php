<?php
/**
 * Child theme of Bono
 * https://wpshop.ru/themes/bono
 *
 * @package Bono
 */

/**
 * Enqueue child styles
 *
 * НЕ УДАЛЯЙТЕ ДАННЫЙ КОД
 */
add_action( 'wp_enqueue_scripts', 'enqueue_child_theme_styles', 100);
function enqueue_child_theme_styles() {
    wp_enqueue_style( 'bono-style-child', get_stylesheet_uri(), array( 'bono-style' )  );
}


/**
 * НИЖЕ ВЫ МОЖЕТЕ ДОБАВИТЬ ЛЮБОЙ СВОЙ КОД
 */